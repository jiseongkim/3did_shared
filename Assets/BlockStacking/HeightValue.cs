﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightValue {
    private float heightValue;

    public float getHeightValue() {
        return this.heightValue;
    }
    public void setHeightValue(float theValue)
    {
        this.heightValue = theValue;
    }


}
