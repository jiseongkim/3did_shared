﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BlockGenerator : MonoBehaviour {

    static public int numOfBlocks;
   
    string[] nameOfCubes;
    
    public GameObject cube;
    protected float y;

    public Ray ray;
    public RaycastHit hitInfo;
    GameObject SelectedBlock;

    static public string blockInfo;

    GameObject[] DestroyingGameObjects;

    int stageLvl;




    void GeneratingBlocks(int numOfBlocks)
    {
        Debug.Log("GeneratingBlocks!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        nameOfCubes = new string[numOfBlocks];

        for (int i = 0; i < numOfBlocks; i++)
        {
            cube = Instantiate(cube) as GameObject;
            float x = Random.Range(-4, 4);
            float z = Random.Range(-4, 4);
            y = Random.Range(0.4f, 1.7f);
            cube.GetComponent<Renderer>().material.color = new Color(Random.value, Random.value, Random.value);
            // cube.renderer.material.color = Color.HSVToRGB(x, x * z, z)();
            cube.transform.localScale = new Vector3(Random.Range(0.4f, 1.7f), y, Random.Range(0.4f, 1.7f));
            cube.transform.position = new Vector3(x, 0.5f, z);
            cube.name = y.ToString();
            nameOfCubes[i] = cube.name;
            Debug.Log(" i  = " + i + "   //   nameOfCubes[i]    =  " + nameOfCubes[i]);

        }

        blockInfo = numOfBlocks + "  Blocks Generated";

        
    }





    void Start () {
        //nextStageButton = nextStageButton.GetComponent<Button>();
        //nextStageButton.onClick.AddListener(TaskOnclick);
        stageLvl = UIDirector.stageLvl;
        numOfBlocks = 6;
        GeneratingBlocks(numOfBlocks);
        

    }
	




	void Update () {
       if (UIDirector.gameover == true ) {
            
            Debug.Log(stageLvl + "  stageLvl  //  "+ UIDirector.stageLvl);
        }
        
    }


    void DestroyAllObjects()
    {
        DestroyingGameObjects = GameObject.FindGameObjectsWithTag("Cube");

        for (var i = 0; i < DestroyingGameObjects.Length; i++)
        {
            Destroy(DestroyingGameObjects[i]);
        }

        DestroyingGameObjects = GameObject.FindGameObjectsWithTag("Stacked");

        for (var i = 0; i < DestroyingGameObjects.Length; i++)
        {
            Destroy(DestroyingGameObjects[i]);
        }
        //for (var i = 0; i < numOfBlocks; i++)
        //{
        //    GameObject dstBlck = GameObject.Find(nameOfCubes[i]);
        //    Destroy(dstBlck);
        //   
        //    Debug.Log(dstBlck+"destropyyyyyyyyyyyyyyyyyyyyed");
        //}
        nameOfCubes = null;
    }

    public void TaskOnclick() {

            Debug.Log(" TaskOnclick()");
            DestroyAllObjects();
            
            //set next level parameter
            GameObject goalobj = GameObject.Find("Goal");
            goalobj.GetComponent<GoalMover>().setGoalPostion(GoalMover.height * stageLvl*0.8f);

            numOfBlocks = Mathf.RoundToInt(numOfBlocks * stageLvl*0.6f);
            GeneratingBlocks(numOfBlocks);

            UIDirector.gameover = false;
            UIDirector.button.SetActive(false);
            UIDirector.stageText.SetActive(false);
            UIDirector.stackHeight = 0.6f;

    }

}
