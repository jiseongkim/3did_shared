﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Academy.HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;

public class BlockController : MonoBehaviour, IInputClickHandler {

    float height;
    float normal;

    //public Scoring sc;


    public Ray ray;
    public RaycastHit hitInfo;
    public GameObject SelectedBlock;

    static public string heightInfo;   //UIDirector - BlockInformation in String
    
    
    //-----------------------Accessor---------------------//
    float posX;
    float posZ;
    //-----------------------Accessor---------------------//


    void Start () {
        heightInfo = "";
        this.height = transform.localScale.y;

    }



    void Update()
     {
        SelectingBlock();
        //Positioning();
        //if (SelectedBlock!=null) {
            parseHeightInfo();
            SetPosX();
            SetPosZ();
        //}
     }

    #region IInputClickHandler
    public void OnInputClicked(InputClickedEventData eventData)
    {
        Debug.Log("Air tappppppped");
        if (gameObject.GetComponent<MeshRenderer>().material.color != Color.green)
        {
            gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 255, 255);
        }
    }

    #endregion IInputClickHandler

    public void SelectingBlock() {
        //Seleting a Block
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity))
            {
                Debug.Log(ray.direction);
                SelectedBlock = GameObject.Find(hitInfo.transform.gameObject.name);
                SelectedBlock.GetComponent<Renderer>().material.color = Color.red;
                
                Debug.Log(SelectedBlock + " is Selected---------Block Controller --------");
            }
            
        }
    }

    //Position manipulation of Block
    public void Positioning()
    {
        //Block postion manipulating
        //this.posX = SelectedBlock.transform.localPosition.x;
        //this.posZ = SelectedBlock.transform.localPosition.z;
    }

    public void SetPosX() {
        this.posX = SelectedBlock.transform.position.x;
    }
    
    public void SetPosZ()
    {
        this.posZ = SelectedBlock.transform.position.z;
    }

    public void parseHeightInfo() {
        //In Order to Access the name/height of Block ----PARSING REQUIRED  WITH  = SelectedBlock.ToString();
        heightInfo = SelectedBlock.ToString();
        heightInfo = heightInfo.Substring(0, heightInfo.LastIndexOf(" ") + 1);
        heightInfo = heightInfo.Replace(" ", "");
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        float massx = this.GetComponent<Rigidbody>().worldCenterOfMass.x;
        float massz = this.GetComponent<Rigidbody>().worldCenterOfMass.z;

        foreach (ContactPoint contact in collision.contacts)
        {

            if (contact.otherCollider.tag == "Base")
            {
                contact.thisCollider.tag = "Cube";
            }else if (contact.otherCollider.tag == "Cornerstone" && massx < 0.5f && massx > -0.5f && massz < 0.5f && massz > -0.5f) {
                contact.thisCollider.tag = "Stacked";
            }else if (contact.otherCollider.tag == "Cube")
            {
                contact.thisCollider.tag = "Cube";
            }else if (contact.otherCollider.tag == "Stacked" && massx < 0.5f && massx > -0.5f && massz < 0.5f && massz > -0.5f)
            {
                contact.thisCollider.tag = "Stacked";
            }
            //Debug.Log(contact.point.z + "++++++++++++contact.point.z++++++=");
            //Debug.Log(contact.point.y + "++++++++++++contact.point.y+++++++=");
           
        }


        //Debug.Log(collision.gameObject.GetComponent<Rigidbody>().mass + "+++++++++++++++++collision.gameObject <Rigidbody>().mass 매스스스스스스+++++++=");
        //Debug.Log(collision.contacts.ToString() + "+++++++++++++++++collision.contacts.ToString()+++++++=");


       
            if (collision.gameObject.tag == "Stacked")
            {

                float scaleY = this.transform.localScale.y;

                //call HeightCalculation in UI Director for adding value to stackHeight
                GameObject stacking = GameObject.Find("UIDirector");
                stacking.GetComponent<UIDirector>().HeightCalculation(scaleY);

            }
        



        //if (posX < 0.5f && posX > -0.5f && posZ < 0.5f && posZ > -0.5f)
        //{
        //    if (collision.gameObject.tag == "Stacked") {
               
        //            float scaleY = this.transform.localScale.y;
            
        //            //call HeightCalculation in UI Director for adding value to stackHeight
        //            GameObject stacking = GameObject.Find("UIDirector");
        //            stacking.GetComponent<UIDirector>().HeightCalculation(scaleY);
                
        //    }
        //}


    }


    private void OnCollisionExit(Collision collision)
    {
        collision.gameObject.GetComponent<Renderer>().material.color = Color.white;
        //collision.transform.SetPositionAndRotation(new Vector3(0, collision.transform.position.y, 0), Quaternion.Euler(0, 0, 0));
        GetComponent<ParticleSystem>().Play();
    }

    //private void OnCollisionStay(Collision collision)
    //{
    //    //check the stack status
    //    Debug.Log(collision.collider.attachedRigidbody.gameObject.ToString()+ "    check the stack status");
    //}

   


}
