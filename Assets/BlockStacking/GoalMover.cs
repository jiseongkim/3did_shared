﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoalMover : MonoBehaviour {

    
    public static float height;
    

   

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Stacked")
        {
            if (UIDirector.stackHeight >= height-0.5 && other.transform.position.y > height - 0.7f)
            {

                Debug.Log(other.gameObject+"Goal Touched!!" +"stage clear from GoalMver");
                GetComponent<Renderer>().material.color = Color.blue;


                //calling JudgeByBlockPosition(GameObject block)
                GameObject calling = GameObject.Find("UIDirector");
                calling.GetComponent<UIDirector>().JudgeByBlockPosition(other.gameObject);
               

            }
        }
    }



    void Start()
    {
        
        height = 3.0f;
        setGoalPostion(height);

    }

    public void setGoalPostion(float height) {
        
            this.gameObject.transform.SetPositionAndRotation(new Vector3(0, height, 0), Quaternion.identity);
            Debug.Log(height + "height-------------------------------");
        
    }


}
