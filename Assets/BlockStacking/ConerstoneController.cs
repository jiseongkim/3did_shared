﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConerstoneController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (this.gameObject.tag != "Cornerstone") { 
            this.gameObject.tag = "Cornerstone";
        }
        if (this.GetComponent<Renderer>().material.color != Color.yellow) { 
            this.GetComponent<Renderer>().material.color = Color.yellow;
        }
    }
}
