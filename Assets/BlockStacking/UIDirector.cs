﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;




public class UIDirector : MonoBehaviour
{

    GameObject noticeText;

    static public GameObject button;
    static public GameObject stageText;
    static public bool gameover;
    public static int stageLvl;
    //Text cleartext;
    //bool gameover;

    

    float time = 30.0f;
    float goalHeight;
    static public float stackHeight;
 
    string blockInfomation;
    GameObject block;
    float massX;
    float massZ;

    void Start()
    {
        this.noticeText = GameObject.Find("Notice");
        gameover = false;
        stageLvl = 1;
        stageText = GameObject.Find("StageText");
        //stageText.GetComponent<Text>().text = "STAGE : " + stageLvl.ToString();
        stackHeight = 0.6f;
        goalHeight = GameObject.Find("Goal").GetComponent<GoalMover>().transform.position.y;
        button = GameObject.Find("Button");

        stageText.SetActive(false);
        button.SetActive(false);
        

    }


    void Update()
    {

        //Time Calculation
        //this.time -= Time.deltaTime;
        //this.timeText.GetComponent<Text>().text = this.time.ToString("F0") + "  Remained";
       

        blockInfomation = BlockController.heightInfo;

        if (blockInfomation == "")
        {
            noticeText.GetComponent<Text>().text = BlockGenerator.blockInfo;
        }else{
            NoticeBlockInfo(blockInfomation);
            //JudgeByBlockPosition(block);
        }


        

    }




    public void NoticeBlockInfo(string blockInfomation)
    {
        if (!blockInfomation.Contains("B") && !blockInfomation.Contains("C") && !blockInfomation.Contains("G"))
        {
            if (Input.GetMouseButton(0))
            {
                noticeText.GetComponent<Text>().text = blockInfomation;
                
                
                //block = GameObject.Find(blockInfomation).GetComponent<BlockController>().gameObject;
                //Debug.Log(block.ToString() + "====================NoticeBlockInfo====at the director class=================");


            }
            else
            {
                noticeText.GetComponent<Text>().text = "";
            }
        }
        else if (blockInfomation.Contains("B") || blockInfomation.Contains("C") || blockInfomation.Contains("G"))
        {
            if (blockInfomation.Contains("B"))
            {
                noticeText.GetComponent<Text>().text = "";
            }
            else if (blockInfomation.Contains("C"))
            {
                if (Input.GetMouseButton(0))
                {
                    noticeText.GetComponent<Text>().text = "CornerStone";
                }
                else
                {
                    noticeText.GetComponent<Text>().text = "";
                }
            }
            else if (blockInfomation.Contains("G"))
            {
                if (Input.GetMouseButton(0))
                {
                    noticeText.GetComponent<Text>().text = "Goal Point";
                }
                else
                {
                    noticeText.GetComponent<Text>().text = "";
                }
            }

        }







    }

    public void JudgeByBlockPosition(GameObject block) {

        //massX = block.GetComponent<Rigidbody>().worldCenterOfMass.x;
        //massZ = block.GetComponent<Rigidbody>().worldCenterOfMass.z;

        //Debug.Log(block.GetComponent<Rigidbody>().worldCenterOfMass + "@@@@@@@@@@@@@@@@@block.GetComponent<Rigidbody>().mass@@@@@@@@@@@@@@@@@@");
        Debug.Log(block + "==============JudgeByBlockPosition(GameObject block)======");
        //Debug.Log(massZ + "======================massZ===========");
        //Debug.Log(block.transform.localScale.y + "+++++++++++++++++block.transform.localScale.y++++++++++++=");
        //Debug.Log(block.ToString() + "++++++++++block.ToString() ++++++++++++++++=");
        //
        //if (block.transform.position.y>= goalHeight - 0.2) {
            if (stackHeight >= goalHeight - 0.2) {

                //stage clear
                Debug.Log(stackHeight +" > "+ goalHeight +"  stage clear from JudgeByBlockPosition in UI Director");
                gameover = true;

                //text get active
                stageText.SetActive(true);
                stageText.GetComponent<Text>().text = "STAGE "+ stageLvl + " CLEAR!! ";

                stageLvl += 1;

                //button get active
                button.SetActive(true);
                button.GetComponentInChildren<Text>().text = "Move onto the STAGE " + stageLvl.ToString();
                
                


            }
        //}

    }

    public void HeightCalculation(float blockHeight) {
        stackHeight += blockHeight;
        Debug.Log(stackHeight + "++++++++++ stackHeight++++++   HeightCalculation    +++=");
        Debug.Log(goalHeight + "++++++++++goalHeight ++++++   HeightCalculation    +++=");
        Debug.Log(blockHeight + "++++++ blockHeight   HeightCalculation    +++=");
    }



}
